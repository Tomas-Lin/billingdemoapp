import { useState } from 'react';

const registerUser = (payload) => {
  return fetch('http://127.0.0.1:8888/users', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload)
  }).then(resp => resp.json())
};

const RegisterForm = ({ refresh }) => {
  const [account, setAccount] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');


  return (
    <form className="pure-form">
      <fieldset>
        <legend>RegisterForm</legend>
        <input type="text" placeholder="Account" value={account} onChange={e => setAccount(e.target.value)} />
        <input type="text" placeholder="Name" value={name} onChange={e => setName(e.target.value)} />
        <input type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} />
        <button type="submit" className="pure-button pure-button-primary" onClick={async () => {
          await registerUser({ account, password, name });
          refresh();
        }}>註冊</button>
      </fieldset>
    </form>
  );
}

export default RegisterForm;
