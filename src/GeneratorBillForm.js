import isEmpty from 'lodash/isEmpty';
import { useState } from 'react';

const generatorBillResult = (userId, amount) => {
  return fetch(`http://127.0.0.1:8888/bills/${userId}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ amount })
  }).then(resp => resp.json())
};


const GeneratorBillForm = ({ users, refreshBills }) => {
  const [userId, setUserId] = useState('');
  const [amount, setAmount] = useState('');

  return (
    <div>
      <select onChange={e => setUserId(e.target.value)}>
        {users.map(user => {
          return (<option value={user.id} key={user.id}>{user.name}</option>);
        })}
      </select>
      <input value={amount} onChange={e => setAmount(e.target.value)} />
      <button onClick={async () => {
        try {
          const id = isEmpty(userId)? users[0].id: userId;
          await generatorBillResult(id, amount);
          await refreshBills();
        } catch (error) {
          console.log('GeneratorBillForm -> error', error)

        }
      }}>建立訂單</button>
    </div>
  );
};

export default GeneratorBillForm;
