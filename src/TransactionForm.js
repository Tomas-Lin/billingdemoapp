import { Fragment, useState } from 'react';

const TransactionForm = ({ service, handleRefreshAccounts }) => {
  const [from, setFrom] = useState('');
  const [to, setTo] = useState('');
  const [value, setValue] = useState('');

  return (
    <Fragment>
      <div>轉帳</div>
      <form className="pure-form">
        <fieldset>
          <legend>TransactionForm</legend>
          <input type="text" placeholder="from" value={from} onChange={e => setFrom(e.target.value)} />
          <input type="text" placeholder="to" value={to} onChange={e => setTo(e.target.value)} />
          <input type="number" placeholder="value" value={value} onChange={e => setValue(e.target.value)} />
          <button type="submit" className="pure-button pure-button-primary" onClick={async (e) => {
            e.preventDefault();
            try {
              const syncData = await service.eth.isSyncing();
              if(syncData !== false) alert(`currentBlock: ${syncData.currentBlock}, highestBlock" ${syncData.highestBlock}`);

              const nonce = await service.eth.getTransactionCount(from);
              const gasPrice = await service.eth.getGasPrice();
              const balance = await service.eth.getBalance(from);
              const workId = await service.eth.net.getId();

              console.log('🚀 ~ file: TransactionForm.js ~ line 36 ~ <buttontype="submit"className="pure-buttonpure-button-primary"onClick={ ~ service.eth.sendTransaction', service.eth.sendTransaction)
              const result = await service.eth.sendTransaction({
                nonce,
                from,
                to,
                gasPrice,
                value: service.utils.toWei(value.toString(), 'ether'),
                gas: "900000",
              }, 'newAccount');
              console.log('🚀 ~ file: TransactionForm.js ~ line 36 ~ <buttontype="submit"className="pure-buttonpure-button-primary"onClick={ ~ result', result)
              handleRefreshAccounts();
            } catch (error) {
              console.log('App -> error', error)
            }
          }}>轉帳</button>
        </fieldset>
      </form>
    </Fragment>
  );
};

export default TransactionForm;
