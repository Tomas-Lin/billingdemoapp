import { useState } from 'react';

const bindAccountResult = (userId, address) => {
  return fetch(`http://10.10.10.4:8888/users/${userId}/bind/${address}`, {
    method: 'POST'
  })
    .then(result => result.json());
};

const BindAddress = ({ userId, refresh }) => {
  const [address, setAddress] = useState('');
  return (
    <span>
      <input value={address} onChange={e => setAddress(e.target.value)} />
      <button onClick={async () => {
        await bindAccountResult(userId, address);
        refresh();
      }}>綁定</button>
    </span>
  );
};

export default BindAddress;
