import { useState } from 'react';

const ShowAccountForm = ({service, saveAccount }) => {
  const [privateKey, setPrivate] = useState('');


  return (
    <form className="pure-form">
      <fieldset>
        <legend>ShowAccountForm</legend>
        <input type="text" placeholder="Address" value={privateKey} onChange={e => setPrivate(e.target.value)} />
        <button type="submit" className="pure-button pure-button-primary" onClick={async () => {
          const account = service.eth.accounts.privateKeyToAccount(privateKey);
          saveAccount(account);
        }}>註冊</button>
      </fieldset>
    </form>
  );
}

export default ShowAccountForm;
