import 'purecss';
import Web3 from 'web3';
import isNull from 'lodash/isNull';
import isEmpty from 'lodash/isEmpty';
import { useEffect, useState } from 'react';
import SocialNetwork from './abis/SocialNetwork.json'
import Identicon from 'identicon.js';

const loadWeb3 = async () => {
  if (window.ethereum) {
    window.web3 = new Web3(window.ethereum)
    await window.ethereum.enable()
    return true;
  } else if (window.web3) {
    window.web3 = new Web3(window.web3.currentProvider)
    return true;
  } else {
    // window.web3 = new Web3(new Web3.providers.HttpProvider(`http://localhost:8545`));
    window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!');
    return false;
  }
};

const loadBlockchainData = async (hasWeb3, setSocialNetworkSol, setPostCount, setPosts, setNetworkId, setAccounts, setPickAccount) => {
  if (hasWeb3) {
    const web3 = window.web3;
    const accounts = await web3.eth.getAccounts();

    if (accounts.length !== 0) {
      setPickAccount(accounts[0]);
    }
    setAccounts(accounts);
    const networkId = await web3.eth.net.getId();
    setNetworkId(networkId);
    const networkData = SocialNetwork.networks[networkId];

    if (networkData) {
      const socialNetwork = new web3.eth.Contract(SocialNetwork.abi, networkData.address);
      setSocialNetworkSol(socialNetwork);
      const postCount = await socialNetwork.methods.postCount().call();
      setPostCount(postCount);
      const postPromises = Array.from({ length: postCount }).map(async (_, i) => {
        const post = await socialNetwork.methods.posts(i).call();
        return post;
      });
      const posts = await Promise.all(postPromises);
      setPosts(posts);
    }
  }
};

const createPost = (socialNetworkSol, account, content) => {
  if (isEmpty(content)) return;

  socialNetworkSol.methods.createPost(content).send({ from: account }).once('receipt', (receipt) => {
  });
}

const tipPost = (socialNetworkSol, account, id, tipAmount) => {
  socialNetworkSol.methods.tipPost(id).send({ from: account, value: tipAmount })
    .once('receipt', (receipt) => {

    });
}
function App() {
  const [socialNetworkSol, setSocialNetworkSol] = useState(null);
  const [postCount, setPostCount] = useState(null);
  const [networkId, setNetworkId] = useState(null);
  const [pickAccount, setPickAccount] = useState([]);
  const [posts, setPosts] = useState([]);
  const [accounts, setAccounts] = useState([]);
  const [content, setContent] = useState('');

  useEffect(() => {
    loadWeb3().then(hasWeb3 => loadBlockchainData(hasWeb3, setSocialNetworkSol, setPostCount, setPosts, setNetworkId, setAccounts, setPickAccount));
  }, []);
  return (
    <div className="App">
      {
        isNull(networkId)
          ? ''
          : <p>connected network: Rinkeby</p>
      }

      <div className="row">
        <main role="main" className="col-lg-12 ml-auto mr-auto" style={{ maxWidth: '500px' }}>
          <div className="content mr-auto ml-auto">
            <p>&nbsp;</p>
            <select value={pickAccount} onChange={e => setPickAccount(e.target.value)}>
              {accounts.map(account => (<option value={account}>{account}</option>))}
            </select>
            <form onSubmit={(event) => {
              event.preventDefault()
              createPost(socialNetworkSol, pickAccount, content);
            }}>
              <div className="form-group mr-sm-2">
                <input
                  type="text"
                  className="form-control"
                  placeholder="What's on your mind?"
                  onChange={e => setContent(e.target.value)}
                  value={content}
                  required />
              </div>
              <button type="submit" className="btn btn-primary btn-block">Share</button>
            </form>
            <p>&nbsp;</p>
            {posts.map((post, key) => {
              return (
                <div className="card mb-4" key={key} >
                  <div className="card-header">
                    <img
                      className='mr-2'
                      width='30'
                      height='30'
                      src={`data:image/png;base64,${new Identicon(post.author, 30).toString()}`}
                    />
                    <small className="text-muted">{post.author}</small>
                  </div>
                  <ul id="postList" className="list-group list-group-flush">
                    <li className="list-group-item">
                      <p>{post.content}</p>
                    </li>
                    <li key={key} className="list-group-item py-2">
                      <small className="float-left mt-1 text-muted">
                        TIPS: {window.web3.utils.fromWei(post.tipAmount.toString(), 'Ether')} ETH
                        </small>
                      <button
                        className="btn btn-link btn-sm float-right pt-0"
                        onClick={() => {
                          const tipAmount = window.web3.utils.toWei('0.1', 'Ether');
                          tipPost(socialNetworkSol, pickAccount, post.id, tipAmount);
                          // this.props.tipPost(event.target.name, tipAmount)
                        }}
                      >
                        TIP 0.1 ETH
                        </button>
                    </li>
                  </ul>
                </div>
              )
            })}
          </div>
        </main>
      </div>
    </div>
  );
}

export default App;
