import { useState } from 'react';

const STATUS_STRING = ['轉帳中', '交易中', '交易成功', '交易失敗'];
const updateBillTransactionHash = (billId, transactionHash) => {
  return fetch(`http://10.10.10.4:8888/bills/${billId}/transaction/${transactionHash}`, {
    method: 'POST'
  })
    .then(result => result.json());
}

const BillListItem = ({ bill, service, handleRefreshAccounts }) => {
  const [address, setAddress] = useState('');

  return (
    <li className="pure-menu-item">
      <a href="#" className="pure-menu-link">to: {bill.user.name}</a>
        amount: {`${bill.amount} eth`}
        <span style={{margin: 5}}>狀態: ${STATUS_STRING[bill.status]}</span>
        <input placeholder='from' onChange={e => setAddress(e.target.value)} value={address} />
        <button className="pure-button pure-button-primary" onClick={async () => {
            try {
              const gasPrice = await service.eth.getGasPrice();

              const result = await service.eth.sendTransaction({
                from: address,
                to: bill.to,
                gasPrice,
                value: service.utils.toWei(bill.amount.toString(), 'ether'),
                gas: "900000",
              });
              await updateBillTransactionHash(bill.id, result.transactionHash);
              
              setTimeout(handleRefreshAccounts, 1000);
            } catch (error) {
              console.log('App -> error', error)
            }
          }}> 轉帳</button>
    </li>
  );
};

export default BillListItem;
